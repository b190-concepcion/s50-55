import { Row, Col, Card, Button } from "react-bootstrap";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

// we can try to destructure the props object recieved from the parent component(Courses.js) so that the main object from the mock database will be accessed
export default function CourseCard({ courseProp }) {
  //console.log(props);
  //console.log(typeof props);

  // destructure the courseProp object for more code readability and ease of coding for the part of the dev (doesn't have to access multiple objects before accessing the needed properties)
  const { name, description, price, _id } = courseProp;

  // const [count, setCount] = useState(0);
  // const [seats, setCount1] = useState(10);
  // const [isOpen, setIsOpen] = useState(true);

  // function enroll() {
  //     setCount(count + 1);
  //     console.log(Enrollees: ${count});
  //     setCount1(seats - 1);
  //     console.log(Seats: ${seats});

  //     // my activity code
  //     // if (count === 10){
  //     //     alert(No more seats)
  //     // } else {
  //     //     setCount(count + 1)
  //     //     console.log(Enrollees: ${count})
  //     // }

  // }

  // useEffect(() => {
  //     if (seats === 0) {
  //     setIsOpen(false);
  //     }
  // }, [seats]);

  // Two-way binding
  // refers to the domino effect of changing the state going into the change of the display/UI in the frontend

  return (
    <Row className="mt-3 mb-3">
      <Col xs={12} md={12}>
        <Card className="cardHighlight p-3">
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
            <Card.Text>{description}</Card.Text>
            <Card.Subtitle>Price:</Card.Subtitle>
            <Card.Text>{price}</Card.Text>
            <Link className="btn btn-primary" to={`/courses/${_id}`}>
              Details
            </Link>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
