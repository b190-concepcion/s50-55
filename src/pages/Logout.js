import { Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import { useContext, useEffect } from "react";

export default function Logout() {
  const { unsetUser, setUser } = useContext(UserContext);

  // clear the local storage
  unsetUser();

  /*
 placing the "setUser" setter function inside of a useEffect is necessary becauase of the updates in React.js that a state of another component cannot be updated whiel trying to render a different component.

 by adding useEffect, this will render Logout oage first before triggering the useEffect which changes the state of the user
*/

  useEffect(() => {
    //Set the user state back to it's original state
    setUser({ id: null });
  });

  return <Navigate to="/login" />;
}
