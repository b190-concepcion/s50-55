import { useState, useEffect, useContext } from "react";
import { Form, Button } from "react-bootstrap";
import { Navigate, useNavigate } from "react-router-dom";

// User Context
import UserContext from "../UserContext";

//sweetalert2
import Swal from "sweetalert2";

export default function Register() {
  //ACTIVITY - S54
  const { user, setUser } = useContext(UserContext);

  /*
  setUser({
    email: localStorage.getItem("email"),
  });
*/
  /*
			Miniactivity 
				1 create email, password1 and password2 variables using useState and set their inital value to empty string ""
				2 create isActive variable using useState and set the initial value to false
				3 refactor the button and add a ternary operator stating that if the isActive is true, the button will be clickable, otherwise, disabled

				6 minutes: 6:30 pm
		*/
  // state hooks to store the values of the input fields
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  // state to determine whether the submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  const navigate = useNavigate();

  // to check if we have successfully implemented the 2-way binding
  /*
			whenever a state of a component changes, the component renders the whole component executing the code found in the component itself.
			e.target.value allows us to gain access the input field's current value to be used when submitting form data
		*/
  console.log(email);
  console.log(password1);
  console.log(password2);
  /*
			Miniactivity 10 minutes
				using useEffect, create a function that will trigger the submit button to be enabled once the conditions have met.
					if:
					- the input fields have to be filled with information from the user
					- password1 and password2 should match
						change the value of setIsActive into true
					-otherwise:
						setIsActive value into false

				the function should listen to email, password1 and password2 variables
				Send the screenshot in the google chat
		*/
  useEffect(() => {
    if (
      firstName !== "" &&
      lastName !== "" &&
      mobileNo !== "" &&
      mobileNo.length > 10 &&
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, mobileNo, email, password1, password2]);

  function registerUser(e) {
    e.preventDefault();

    // clear input fields
    /*
			setter functions can also bring back the original states of the getter variables if the dev wants to
		*/
    setFirstName("");
    setLastName("");
    setMobileNo("");
    setEmail("");
    setPassword1("");
    setPassword2("");

    fetch("http://localhost:4000/users/checkEmail", {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data === false) {
          fetch("http://localhost:4000/users/register", {
            method: "POST",
            headers: {
              "Content-type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              mobileNo: mobileNo,
              email: email,
              password: password1,
            }),
          })
            .then((res) => res.json())
            //good practice to output the response in hte console to check if the codes are working
            .then((data) => {
              console.log(data);

              if (data === true) {
                Swal.fire({
                  title: "Registration Successful",
                  icon: "success",
                  text: "Welcome to Zuitt",
                });
                navigate("/login");
              } else {
                Swal.fire({
                  title: "Authentication failed",
                  icon: "error",
                  text: "Check your login credentials and try again.",
                });
              }
            });
        } else {
          Swal.fire({
            title: "Duplicate Email Found",
            icon: "error",
            text: "Enter a Valid Email Address",
          });
        }
      });
    //alert("Thank you for registering!");
  }

  return user.id !== null ? (
    <Navigate to="/courses" />
  ) : (
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="string"
          placeholder="First Name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="string"
          placeholder="Last Name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="mobileNumber">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="number"
          placeholder="Mobile Number"
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>
      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="primary" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
